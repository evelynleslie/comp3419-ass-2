PVector gravity;   
boolean clicked = false;

PImage blue;
PImage purple;

ArrayList <PShape> arr;
ArrayList<PVector> loc;
ArrayList<PVector> velo;

void setup() {
  size(400, 400, P3D);
  blue = loadImage("menu.jpg");
  purple = loadImage("bckg.png");
  
  gravity = new PVector(0.2,0.2); 
  
  arr = new ArrayList<PShape>();
  loc = new ArrayList<PVector>();
  velo = new ArrayList<PVector>();
}

void draw() {

  background(255,204,0);
  translate(width/2,height/2,-200); //position is inside (in the middle of) the box
  stroke(255);
  strokeWeight(2);
  noFill();
  
  box(400);
  noStroke();
  for(int i = 0; i < arr.size(); i++){
    
  loc.get(i).add(velo.get(i));  
  
  //if x coordinate overlaps the left or right wall
  if ((loc.get(i).x > 200) || (loc.get(i).x < -1*200)) {
    //switch direction
    velo.get(i).x = velo.get(i).x * -1;  
    //add gravity
    velo.get(i).x *= 0.8;
    if(loc.get(i).x > 200){
      loc.get(i).x = 200;
    }
    else{
      loc.get(i).x = -200;
    }
  }
  //if y coordinate overlaps the top or bottom wall
  if (loc.get(i).y > 200 || loc.get(i).y < -200) {
    //switch direction
    velo.get(i).y = velo.get(i).y * -1; 
    //add gravity
    velo.get(i).y *= 0.6;
    if(loc.get(i).y > 200){
      loc.get(i).y = 200;
    }
    else{
      loc.get(i).y = -200;
    }
  }
  
  //if y coordinate overlaps the wall behind
  if(loc.get(i).z < -200 ){
    //switch direction
    velo.get(i).z *= -1;
  }
  
  pushMatrix();
    translate(loc.get(i).x,loc.get(i).y,loc.get(i).z);
    shape(arr.get(i)); 
    popMatrix();
  }
  
  if(clicked == true){
    //creates a ball every time the mouse is clicked
    PShape tmp;
    tmp = createShape(SPHERE,50);
    tmp.setTexture(random());
    arr.add(tmp); 
    clicked = false;
  }

}

void mouseClicked() {
  fill(255, 255, 255);
  //gets the mouse's coordinates for the balls' positions
  //position is inside (in the middle of) the box
  int x = mouseX - 200;
  int y = mouseY - 200;
  PVector location = new PVector(x,y,50);
  loc.add(location);
  PVector velocity = new PVector(directionX(),directionY(),-10);
  velo.add(velocity);
  clicked = true;
}

int r(){
  int r = (int)random(0,2);
  return r;
}

//x speed
float directionX(){
  int r = r();
  if(r == 0){
    return -5.5;
  }
  else{
    return 5.5;
  }
}

//y speed
float directionY(){
  int r = r();
  if(r == 0){
    return -10.5;
  }
  else{
    return 10.5;
  }
}

//randomise the ball's texture
PImage random(){
  int r = r();
  if(r == 0){
    return blue;
  }
  else{
    return purple;
  }
}